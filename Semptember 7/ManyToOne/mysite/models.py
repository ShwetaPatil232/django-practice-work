from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post_name = models.CharField(max_length=50)
    post_type = models.CharField(max_length=50)
    publish_date = models.DateField()

    '''
        This models shows ManyToOne Retionship among Post and User
        1) User can create Many Post. There is no restrictions
            but if we delete User then all Posts also autometically deleted
            
            1) Go to 127.0.0.1/admin and login
            2) First Create Users as many as you want.
            3) Each User can create Post as they want. [without Restrictions] 
                [ ManyToOne : Many Post Created by One User is Allowed. ]  
    '''