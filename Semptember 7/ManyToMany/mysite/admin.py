from django.contrib import admin
from .models import Songs

# Register your models here.

@admin.register(Songs)
class SongsAdmin(admin.ModelAdmin):
    list_display = [ 'song_name', 'song_duration', 'writtern_by']