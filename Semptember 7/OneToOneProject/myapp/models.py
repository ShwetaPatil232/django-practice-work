from django.db import models
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Page(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    page_name = models.CharField(max_length=50)
    page_type = models.CharField(max_length=50)
    page_publish_date = models.DateField()

    '''
        This model contain OneToOneFiled with User. SO Each User is Only create 1 Post
        Otherwise it shows Error
        
       1) Go to  on : 127.0.0.1/admin Then Login 
       2) First Create User Before Post.
       3) After that create Post.  a) Each User allow to create only 1 Post
                                   b) Otherwise it Shows Error..
    '''
