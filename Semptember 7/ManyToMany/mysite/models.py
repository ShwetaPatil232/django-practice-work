from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Songs(models.Model):
    user = models.ManyToManyField(User)
    song_name = models.CharField(max_length=50)
    song_duration =  models.IntegerField()

    def writtern_by(self):
        return ",".join( [str(p) for p in self.user.all()] )

    '''
            This model is used  ManytoManyField which allow us to create model relation 
            that consist : Many songs are sung by many Users.
            
            1) Go to 127.0.0.1/admin and login
            2) First Create Users as many as you want
            3) Then create Many Songs which are sung by Many Users 
    '''